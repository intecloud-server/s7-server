package com.whl.project.service.component.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MqttConnectionComponent implements InitializingBean {
	@Autowired
	MqttClient mqttClient;
	@Autowired
	MqttConnectOptions mqttConnectOptions;
	@Autowired
	MqttCallbackComponent mqttCallbackComponent;

	@Override
	public void afterPropertiesSet() throws Exception {
		mqttClient.setCallback(mqttCallbackComponent);
		mqttClient.connect(mqttConnectOptions);
	}

}
