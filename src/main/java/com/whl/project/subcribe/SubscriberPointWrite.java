package com.whl.project.subcribe;

import java.util.List;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.whl.ActionInfo;
import com.whl.common.constants.AppEnumType.ActionExeType;
import com.whl.plc.PlcWriteValueInfo;
import com.whl.project.properties.GlobalProperites;
import com.whl.project.protocol.AdapterExecutor;
import com.whl.project.protocol.LocalCacheUtil;
import com.whl.project.service.component.S7Component;
import com.whl.project.service.component.mqtt.MqttPublishComponent;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SubscriberPointWrite implements ISubcribe {
	@Autowired
	@Qualifier("defaultThreadPool")
	ThreadPoolTaskExecutor threadPoolTaskExecutor;
	@Autowired
	private GlobalProperites globalProperites;
	@Autowired
	private MqttPublishComponent mqttPublishComponent;
	@Autowired
	private S7Component s7Component;

	@Override
	public void dealMsg(String topic, String msg) throws Exception {
		String exeResultTopic = "exeResult";
		ActionInfo aInfo = new ActionInfo();

		String projectPrefix = projectPrefix();
		String adapterCode = topic.substring(projectPrefix.length());
		PlcWriteValueInfo wValueInfo = JSON.parseObject(msg, PlcWriteValueInfo.class);
		String actionSn = wValueInfo.getActionSn();
		aInfo.setActionSn(actionSn);
		// 获取执行器
		AdapterExecutor adapterExecutor = LocalCacheUtil.getAdapterExecutor(adapterCode);
		if (adapterExecutor == null) {
			aInfo.setResult(ActionExeType.fail.code());
			aInfo.setMsg(adapterCode + "找不到执行器");
			String exeResultMsg = JSON.toJSONString(aInfo);
			mqttPublishComponent.execute(0, false, exeResultTopic, exeResultMsg);
			return;
		}
		// 获取写操作
		try {
			boolean isSuc = s7Component.write(adapterExecutor, wValueInfo);
			if(isSuc) {
				aInfo.setResult(ActionExeType.success.code());
			}else {
				aInfo.setResult( ActionExeType.fail.code());
			}
		} catch (Exception e) {
			aInfo.setResult(ActionExeType.fail.code());
			aInfo.setMsg(e.getMessage());
			log.error("写数据异常", e);
		}
		String exeResultMsg = JSON.toJSONString(aInfo);
		mqttPublishComponent.execute(0, false, exeResultTopic, exeResultMsg);
	}

	@Override
	public void subTopic(MqttClient mqttClient) throws Exception {
		String projectPrefix = projectPrefix();
		List<String> adapterCodes = globalProperites.adapterCodes();
		for (String adapterCode : adapterCodes) {
			String topic = projectPrefix + adapterCode;
			log.info("订阅主题：" + topic);
			mqttClient.subscribe(topic, 0);
		}
	}

	@Override
	public String projectPrefix() throws Exception {
		String projectPrefix = SubcribeFactory.WRITE;
		return projectPrefix;
	}

}
