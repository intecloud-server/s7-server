package com.test;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Date;

import com.whl.common.util.DateUtil;
import com.whl.common.util.TxtUtil;

public class TxtTest {

	public static void main(String[] args) throws Exception {
		String path = "abcd.txt";
		String dateStr = DateUtil.date2Str(new Date(), "yyyy-MM-dd HH:mm", false);
		StringBuilder sb = new StringBuilder("======" + dateStr + "======" + TxtUtil.END_FLAG);
		try {
			// 打开一个随机访问文件流，按读写方式
			RandomAccessFile randomFile = new RandomAccessFile(path, "rw");
			// 文件长度，字节数
			long fileLength = randomFile.length();
			// 将写文件指针移到文件尾。
			randomFile.seek(fileLength);
			randomFile.writeUTF(sb.toString() + "\r\n");
			randomFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
