package com.whl.project.subcribe;

import java.util.List;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.whl.project.properties.GlobalProperites;
import com.whl.project.service.component.PointReadComponent;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SubscriberPointRead implements ISubcribe {
	@Autowired
	private GlobalProperites globalProperites;
	@Autowired
	private PointReadComponent pointReadComponent;

	@Override
	public void dealMsg(String topic, String msg) throws Exception {
		String projectPrefix = projectPrefix();
		String adapterCode = topic.substring(projectPrefix.length());
		String deviceCode = msg;
		pointReadComponent.excute(adapterCode, deviceCode);
	}

	@Override
	public void subTopic(MqttClient mqttClient) throws Exception {
		String projectPrefix = projectPrefix();
		List<String> adapterCodes = globalProperites.adapterCodes();
		for (String adapterCode : adapterCodes) {
			String topic = projectPrefix + adapterCode;
			log.info("订阅主题：" + topic);
			mqttClient.subscribe(topic, 0);
		}
	}

	@Override
	public String projectPrefix() throws Exception {
		String projectPrefix = SubcribeFactory.READ;
		return projectPrefix;
	}
}
