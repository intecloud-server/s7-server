package com.whl.project.protocol;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

public class LocalCacheUtil {

	private static Map<String, AdapterExecutor> adapterExecutorMap;
	private static Integer lockObj = 1;

	public static Map<String, AdapterExecutor> getadapterExecutorMap() {
		return adapterExecutorMap;
	}

	public static AdapterExecutor getAdapterExecutor(String adapterCode) {
		if (MapUtils.isEmpty(adapterExecutorMap)) {
			return null;
		}
		AdapterExecutor ret = adapterExecutorMap.get(adapterCode);
		return ret;
	}

	public static void putAdapterExecutor(String adapterCode, AdapterExecutor adapterExecutor) {
		if (adapterExecutorMap == null) {
			synchronized (lockObj) {
				if (adapterExecutorMap == null) {
					adapterExecutorMap = new HashMap<>();
				}
			}
		}
		adapterExecutorMap.put(adapterCode, adapterExecutor);
	}

	public static void removeAdapterExecutor(String adapterCode) {
		if (MapUtils.isEmpty(adapterExecutorMap)) {
			return;
		}
		adapterExecutorMap.remove(adapterCode);
	}

}
