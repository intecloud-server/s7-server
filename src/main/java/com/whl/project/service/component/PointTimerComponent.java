package com.whl.project.service.component;

import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.whl.project.protocol.AdapterExecutor;
import com.whl.project.protocol.LocalCacheUtil;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PointTimerComponent {
	@Autowired
	PointReadComponent pointReadComponent;

	public void start(String adapterCode, boolean isRelay) throws Exception {
		AdapterExecutor adapterExecutor = LocalCacheUtil.getAdapterExecutor(adapterCode);
		int relay = 0;
		if (isRelay) {
			relay = 30_000;
		}
		if (adapterExecutor == null) {
			log.info("adapterExecutor is null");
			return;
		}
		Timer timer = adapterExecutor.getTimer();
		if (timer != null) {
			timer.cancel();
		}
		int pollingTime = adapterExecutor.getPollingTime();
		// 定时器
		timer = new Timer();
		adapterExecutor.setTimer(timer);
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				pointReadComponent.excute(adapterCode, null);
			}
		}, relay, pollingTime * 1_000);
	}

}
