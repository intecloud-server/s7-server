package com.whl.project.service.component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.xingshuangs.iot.protocol.s7.enums.EPlcType;
import com.github.xingshuangs.iot.protocol.s7.model.DataItem;
import com.github.xingshuangs.iot.protocol.s7.model.RequestItem;
import com.github.xingshuangs.iot.protocol.s7.service.S7PLC;
import com.whl.common.constants.AppEnumType;
import com.whl.common.constants.AppEnumType.DataType;
import com.whl.common.exception.BusinessException;
import com.whl.common.util.S7Utils;
import com.whl.plc.PlcPointValueInfo;
import com.whl.plc.PlcWriteValueInfo;
import com.whl.project.properties.GlobalProperites;
import com.whl.project.protocol.AdapterExecutor;
import com.whl.project.service.component.mqtt.MqttPublishComponent;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class S7Component {
	@Autowired
	GlobalProperites globalProperites;
	@Autowired
	MqttPublishComponent mqttPublishComponent;

	public boolean write(AdapterExecutor adapterExecutor, PlcWriteValueInfo wValueInfo) throws Exception {
		Map<String, List<PlcPointValueInfo>> pointMap = adapterExecutor.getPointMap();
		if (MapUtils.isEmpty(pointMap)) {
			throw new BusinessException("找不到点信息");
		}
		List<PlcPointValueInfo> points = pointMap.get(wValueInfo.getIp());
		PlcPointValueInfo mpValueInfo = null;
		if (CollectionUtils.isEmpty(points)) {
			throw new BusinessException("找不到控制器的点位信息");
		}
		mpValueInfo = points.stream().filter(p -> wValueInfo.getAddress().equals(p.getAddress())).findAny()
				.orElse(null);
		if (mpValueInfo == null) {
			throw new BusinessException("找不到点位信息");
		}
		if (!mpValueInfo.getWriteable()) {
			throw new BusinessException("点位不能写");
		}
		Double value = wValueInfo.getPointValue().doubleValue();
		if (mpValueInfo.getRate() != null && mpValueInfo.getRate() > 1) {
			value = value * mpValueInfo.getRate();
		}
		String plcType = mpValueInfo.getEplcType();
		EPlcType ePlcType = EPlcType.valueOf(plcType);
		S7PLC s7PLC = new S7PLC(ePlcType, mpValueInfo.getIp());
		DataType dataType = AppEnumType.getByCode(DataType.class, mpValueInfo.getDataType());
		String address = mpValueInfo.getAddress();
		switch (dataType) {
		case bool:
			s7PLC.writeBoolean(address, value.intValue() == 1 ? true : false);
			break;
		case double64:// String类型值
			s7PLC.writeFloat64(address, value);
			break;
		case float32:
			s7PLC.writeFloat32(address, value.floatValue());
			break;
		case int32: // 读到int类型
			s7PLC.writeInt32(address, value.intValue());
			break;
		case short16:
			s7PLC.writeInt16(address, value.shortValue());
			break;
		default:
			throw new BusinessException(dataType + "暂不支持的数据类型");
		}
		return true;
	}

	public void read(AdapterExecutor adapterExecutor, String controllerIp, List<PlcPointValueInfo> points) {
		if (CollectionUtils.isEmpty(points)) {
			log.warn("找不到点位信息");
			return;
		}
		String plcType = points.get(0).getEplcType();
		EPlcType ePlcType = EPlcType.valueOf(plcType);
		S7PLC s7PLC = new S7PLC(ePlcType, controllerIp);
		List<RequestItem> requestItems = new ArrayList<>();
		for (PlcPointValueInfo point : points) {
			RequestItem requestItem = S7Utils.getRequestItem(point.getDataType(), point.getAddress());
			requestItems.add(requestItem);
		}
		List<DataItem> dataItems = s7PLC.readS7Data(requestItems);
		for (int i = 0; i < dataItems.size(); i++) {
			PlcPointValueInfo point = points.get(i);
			DataItem dataItem = dataItems.get(i);
			Double pointValue = S7Utils.getVal(point.getDataType(), dataItem);
			dealValue(point, pointValue);
		}
	}

	public static void dealValue(PlcPointValueInfo pValueInfo, Double pointValue) {
		if (pointValue == null) {
			log.warn("点位读不到点值");
			return;
		}
		BigDecimal value = null;
		if (pValueInfo.getRate() != null && pValueInfo.getRate() > 1) {
			value = new BigDecimal(pointValue).divide(new BigDecimal(pValueInfo.getRate()));
		} else {
			value = new BigDecimal(pointValue);
		}
		if (pValueInfo.getDotNum() != null && pValueInfo.getDotNum() > 1) {
			value = value.setScale(pValueInfo.getDotNum(), BigDecimal.ROUND_HALF_UP);
		}
		pValueInfo.setPointValue(value);
	}

}
