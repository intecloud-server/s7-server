package com.whl.project.protocol;

import java.util.List;
import java.util.Map;
import java.util.Timer;

import com.whl.plc.PlcPointValueInfo;

import lombok.Data;

@Data
public class AdapterExecutor {
	private Integer pollingTime;
	private Timer timer;
	private Map<String, List<PlcPointValueInfo>> pointMap;
}
