package com.whl.config;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.whl.project.properties.GlobalProperites;

@Configuration
public class BeanConfig {
	@Autowired
	private GlobalProperites globalProperites;

	@Bean
	public String app() {
		String app = "s7-adapter";
		return app;
	}

	@Bean
	public MqttConnectOptions mqttConnectOptions() throws Exception {
		String username = globalProperites.mqttUsername();
		String password = globalProperites.mqttPassword();
		Integer timeout = globalProperites.mqttTimeout();
		Integer keepalive = globalProperites.mqttKeepalive();
		MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
		mqttConnectOptions.setCleanSession(true);
		if (StringUtils.isNotBlank(username)) {
			mqttConnectOptions.setUserName(username);
		}
		if (StringUtils.isNotBlank(password)) {
			mqttConnectOptions.setPassword(password.toCharArray());
		}
		mqttConnectOptions.setConnectionTimeout(timeout);
		mqttConnectOptions.setKeepAliveInterval(keepalive);
		mqttConnectOptions.setAutomaticReconnect(true);
		mqttConnectOptions.setMaxInflight(200);
		String clientId = globalProperites.mqttClientId();
		mqttConnectOptions.setWill("willTopic", (clientId + "与服务器断开连接").getBytes(), 0, false);
		return mqttConnectOptions;
	}

	@Bean
	public MqttClient mqttClient(MqttConnectOptions mqttConnectOptions) throws Exception {
		String host = globalProperites.mqttHost();
		String clientId = globalProperites.mqttClientId();
		MqttClient mqttClient = new MqttClient(host, clientId, new MemoryPersistence());
		return mqttClient;
	}

}
