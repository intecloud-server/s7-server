package com.whl.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ThreadPoolConfig {
	private static final int CUSTOM_COREPOOLSIZE = 200;

	@Bean(name = "defaultThreadPool")
	public ThreadPoolTaskExecutor defaultThreadPool() {
		return customThreadPool("defaultThreadPool");
	}

	private ThreadPoolTaskExecutor customThreadPool(String threadPoolName) {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(CUSTOM_COREPOOLSIZE);
		executor.setThreadNamePrefix(threadPoolName);
		executor.afterPropertiesSet();
		return executor;
	}
}
