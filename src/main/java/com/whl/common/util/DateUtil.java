package com.whl.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	private static final int second = 1000;
	private static final int minute = 60 * second;
	private static final int hour = 60 * minute;
	private static final int day = 24 * hour;

	/**
	 * @throws ParseException
	 */
	public static String date2Str(Date date, String formate, boolean show) {
		if (date == null) {
			return "";
		}
		DateFormat dateFormat = new SimpleDateFormat(formate);
		String format = null;
		long cur = System.currentTimeMillis();
		if (show) {
			long dd = cur - date.getTime();
			if (dd < 1 * 1 * hour) {
				format = "小于1小时";
			} else if (dd < 1 * day) {
				format = "今天";
			} else if (dd < 2 * day) {
				format = "1天前";
			} else if (dd < 3 * day) {
				format = "2天前";
			} else if (dd < 4 * day) {
				format = "3天前";
			} else if (dd < 5 * day) {
				format = "4天前";
			} else if (dd < 6 * day) {
				format = "5天前";
			} else {
				format = dateFormat.format(date);
			}
		} else {
			format = dateFormat.format(date);
		}
		return format;
	}

	public static void main(String[] args) {
		//		"yyyy-MM-dd HH mm"
		System.out.println(date2Str(new Date(), "yyyy-MM-dd HH:mm", false));
	}

}
