package com.whl.event;

import org.springframework.context.ApplicationEvent;

public class WhlApplicationEvent<T> extends ApplicationEvent {
	
	private static final long serialVersionUID = 1L;

	public WhlApplicationEvent(T source) {
		super(source);
	}
	
	@SuppressWarnings("unchecked")
	public T getWhlApplicationModel() {
		return (T) getSource();
	}
	
	
	

}