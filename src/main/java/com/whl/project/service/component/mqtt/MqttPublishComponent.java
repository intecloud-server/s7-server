package com.whl.project.service.component.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MqttPublishComponent {
	@Autowired
	MqttClient mqttClient;

	public synchronized void execute(int qos, boolean retained, String topic, String pushMessage) throws Exception {
		MqttMessage message = new MqttMessage();
		message.setQos(qos);
		message.setRetained(retained);
		message.setPayload(pushMessage.getBytes());
		MqttTopic mTopic = mqttClient.getTopic(topic);
		MqttDeliveryToken mqttDeliveryToken = mTopic.publish(message);
		log.info(mqttDeliveryToken.getMessageId() + "提交消息发布！");
		mqttDeliveryToken.waitForCompletion(3000L);
	}

}
