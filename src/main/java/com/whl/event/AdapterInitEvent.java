package com.whl.event;

import org.springframework.context.ApplicationEvent;

public class AdapterInitEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;

	public AdapterInitEvent(Object source) {
		super(source);
	}
}