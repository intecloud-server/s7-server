package com.whl.common.util;

import java.io.IOException;
import java.io.RandomAccessFile;

public class TxtUtil {
	public static final String END_FLAG = "\r\n";
	public static void write(String path, String content) throws Exception {
		try {
			// 打开一个随机访问文件流，按读写方式
			RandomAccessFile randomFile = new RandomAccessFile(path, "rw");
			// 文件长度，字节数
			long fileLength = randomFile.length();
			// 将写文件指针移到文件尾。
			randomFile.seek(fileLength);
			content =  content + END_FLAG;
			randomFile.write(content.getBytes("UTF-8"));
			randomFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
