package com.test;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import com.whl.common.util.DateUtil;

public class TimerTest {

	public static void main(String[] args) throws Exception {

		Timer timer = new Timer();
		
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				System.out.println(DateUtil.date2Str(new Date(), "yyyy-MM-dd HH:mm:ss", false) + "  execute task!"
						+ this.scheduledExecutionTime());
				try {
					Thread.sleep(6000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}, 0, 3 * 1000);
	}

}
