package com.whl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.whl.event.AdapterInitEvent;
import com.whl.project.SpringUtil;

import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.whl")
@EnableScheduling
@Slf4j
public class Application {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
		log.info("(♥◠‿◠)ﾉﾞ  启动成功   ლ(´ڡ`ლ)");
		SpringUtil.publishEvent(new AdapterInitEvent("初始化适配器"));
	}

}
