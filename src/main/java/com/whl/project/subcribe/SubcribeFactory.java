package com.whl.project.subcribe;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class SubcribeFactory {
	public static final String CONF = "CONF:";
	public static final String READ = "READ:";
	public static final String WRITE = "WRITE:";

	@Autowired
	SubscriberConf subscriberConf;
	@Autowired
	SubscriberPointRead subscriberPointRead;
	@Autowired
	SubscriberPointWrite subscriberPointWrite;
	@Autowired
	MqttClient mqttClient;

	public ISubcribe getSubcribe(String topic) {
		if (topic.startsWith(CONF)) {
			return subscriberConf;
		}
		if (topic.startsWith(READ)) {
			return subscriberPointRead;
		}
		if (topic.startsWith(WRITE)) {
			return subscriberPointWrite;
		}
		return null;
	}

	public void subAllTopic() {
		try {
			subscriberConf.subTopic(mqttClient);
		} catch (Exception e) {
			log.error(subscriberConf.getClass().getSimpleName() + "异常", e);
		}
		try {
			subscriberPointRead.subTopic(mqttClient);
		} catch (Exception e) {
			log.error(subscriberPointRead.getClass().getSimpleName() + "异常", e);
		}
		try {
			subscriberPointWrite.subTopic(mqttClient);
		} catch (Exception e) {
			log.error(subscriberPointWrite.getClass().getSimpleName() + "异常", e);
		}
	}

}
