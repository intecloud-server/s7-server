package com.whl.project.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping
@RestController
public class IndexController {

	@GetMapping("/")
	public String welcome() throws Exception {
		return "你好";
	}

}
