package com.whl.project.service.component;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.whl.plc.PlcPointValueInfo;
import com.whl.project.properties.GlobalProperites;
import com.whl.project.protocol.AdapterExecutor;
import com.whl.project.protocol.LocalCacheUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author zhang
 *
 */
/**
 * @author zhang
 *
 */
@Component
@Slf4j
public class PointReadComponent {
	@Autowired
	S7Component s7Component;
	@Autowired
	GlobalProperites globalProperites;
	@Autowired
	PointPublishComponent pointPublishComponent;

	public void excute(String adapterCode, String deviceCode) {
		log.info(adapterCode+ "开始执行查询");
		AdapterExecutor adapterExecutor = LocalCacheUtil.getAdapterExecutor(adapterCode);
		Map<String, List<PlcPointValueInfo>> pointMap = adapterExecutor.getPointMap();
		if (MapUtils.isEmpty(pointMap)) {
			log.info("找不到点信息：" + adapterCode);
			return;
		}
		Set<String> controllerIps = pointMap.keySet();
		String adapterMsg = "适配器编号：" + adapterCode + ",";
		for (String controllerIp : controllerIps) {
			String controllerIpMsg = adapterMsg + "controllerIp：" + controllerIp + ",";
			try {
				List<PlcPointValueInfo> points = pointMap.get(controllerIp);
				if (CollectionUtils.isEmpty(points)) {
					continue;
				}
				if (StringUtils.isNotBlank(deviceCode)) {
					points = points.stream().filter(e -> e.getDeviceCode().equals(deviceCode))
							.collect(Collectors.toList());
					if (CollectionUtils.isEmpty(points)) {
						continue;
					}
				}
				try {
					s7Component.read(adapterExecutor, controllerIp, points);
					log.info("结束执行查询");
					pointPublishComponent.publish(points);
					log.info("结束发布消息");
				} catch (Exception e) {
					log.error(controllerIpMsg + "读取异常", e);
				}
			} catch (Exception e2) {
			}
		}
	}

}
