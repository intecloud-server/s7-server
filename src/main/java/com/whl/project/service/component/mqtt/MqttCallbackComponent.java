package com.whl.project.service.component.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.whl.project.subcribe.ISubcribe;
import com.whl.project.subcribe.SubcribeFactory;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MqttCallbackComponent implements MqttCallback, MqttCallbackExtended {
	@Autowired
	SubcribeFactory subcribeFactory;

	@Override
	public void connectionLost(Throwable throwable) {
		log.error("连接断开");// 连接丢失
	}

	@Override
	public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
		byte[] payload = mqttMessage.getPayload();
		String mqttMessagePayload = new String(payload);// 接收消息内容(转为字符串格式)
		// 订阅后得到的消息会执行到这里面
		log.info("接收消息主题 : " + topic);
		log.info("接收消息Qos : " + mqttMessage.getQos());
		log.info("接收消息内容 : " + mqttMessagePayload);
		ISubcribe subcribe = subcribeFactory.getSubcribe(topic);
		if (subcribe == null) {
			log.info("没有找到对应的处理类 : " + topic);
			return;
		}
		subcribe.dealMsg(topic, mqttMessagePayload);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

	}

	@Override
	public void connectComplete(boolean reconnect, String serverURI) {
		log.info("连接完成");
		subcribeFactory.subAllTopic();
		log.info("订阅主题完成");
	}

}
