package com.whl.project.model;

import java.util.List;
import java.util.Map;

import com.whl.plc.PlcPointValueInfo;

import lombok.Data;

@Data
public class AdapterConfInfo {
	private Integer pollingTime;
	private Map<String, Object> map;
	private List<PlcPointValueInfo> list;

}
