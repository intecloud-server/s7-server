package com.whl.event;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.whl.project.properties.GlobalProperites;
import com.whl.project.service.AdapterService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class StartEventListener implements ApplicationListener<AdapterInitEvent> {
	@Autowired
	AdapterService adapterService;
	@Autowired
	GlobalProperites globalProperites;

	@Override
	public void onApplicationEvent(AdapterInitEvent event) {
		log.info("初始化开始");
		List<String> adapterCodes = globalProperites.adapterCodes();
		for (int i = 0; i < adapterCodes.size(); i++) {
			// 后面的缓时执行
			boolean isRelay = (i > 0);
			String adapterCode = adapterCodes.get(i);
			try {
				adapterService.load(adapterCode, isRelay);
			} catch (Exception e) {
				log.error(adapterCode + "适配器初始化失败", e);
			}
		}
		log.info("初始化结束");
	}
}
