package com.whl.project.properties;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class GlobalProperites {
	@Autowired
	private Environment env;

	public String projectCode() {
		String ret = env.getProperty("projectCode");
		return ret;
	}

	public String intecloudServer() {
		String ret = env.getProperty("intecloud.server");
		return ret;
	}

	public String adapterImage() {
		String ret = env.getProperty("adapter.image");
		return ret;
	}

	public List<String> adapterCodes() {
		String adapterCodes = env.getProperty("adapterCodes");
		String[] adapterCodeArr = adapterCodes.split(",");
		List<String> ret = Arrays.stream(adapterCodeArr).collect(Collectors.toList());
		return ret;
	}

	public Boolean mqttEnable() {
		Boolean value = env.getProperty("mqtt.enable", Boolean.class);
		return value;
	}

	public String mqttHost() {
		String value = env.getProperty("mqtt.host", String.class);
		return value;
	}

	public String mqttClientId() {
		String value = env.getProperty("mqtt.clientId", String.class);
		return value;
	}

	public String mqttUsername() {
		String value = env.getProperty("mqtt.username", String.class);
		return value;
	}

	public String mqttPassword() {
		String value = env.getProperty("mqtt.password", String.class);
		return value;
	}

	public Integer mqttTimeout() {
		Integer value = env.getProperty("mqtt.timeout", Integer.class);
		return value;
	}

	public Integer mqttKeepalive() {
		Integer value = env.getProperty("mqtt.keepalive", Integer.class);
		return value;
	}

	public String mqttTopic() {
		String value = env.getProperty("mqtt.topic", String.class);
		return value;
	}

}
