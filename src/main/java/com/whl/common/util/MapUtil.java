package com.whl.common.util;

import java.util.Map;
import java.util.TreeMap;

public class MapUtil {
	public static Map<Integer, String> sort(Map<Integer, String> map) {
		if (map == null || map.isEmpty()) {
			return null;
		}
		Map<Integer, String> sortMap = new TreeMap<Integer, String>();
		sortMap.putAll(map);
		return sortMap;
	}

}
