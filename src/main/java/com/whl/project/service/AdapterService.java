package com.whl.project.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.whl.common.dto.ResponseInfo;
import com.whl.common.util.HttpUtil;
import com.whl.plc.PlcPointValueInfo;
import com.whl.project.model.AdapterConfInfo;
import com.whl.project.properties.GlobalProperites;
import com.whl.project.protocol.AdapterExecutor;
import com.whl.project.protocol.LocalCacheUtil;
import com.whl.project.service.component.PointTimerComponent;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AdapterService {
	@Autowired
	GlobalProperites globalProperites;
	@Autowired
	PointTimerComponent pointTimerComponent;

	public synchronized void load(String adapterCode, boolean isRelay) throws Exception {
		String projectCode = globalProperites.projectCode();
		String getPointsUrl = globalProperites.intecloudServer() + "/api/adapter/getAdapterConf";
		Map<String, String> reqMap = new HashMap<>();
		reqMap.put("projectCode", projectCode);
		reqMap.put("adapterCode", adapterCode);
		String respBody = HttpUtil.get(getPointsUrl, null, reqMap);
		ResponseInfo respInfo = JSON.parseObject(respBody, new TypeReference<ResponseInfo>() {
		});
		Assert.isTrue(respInfo.getCode() == 200, respInfo.getMsg());
		if (respInfo.getResult() == null) {
			log.error("projectCode:" + projectCode);
			log.error("adapterCode:" + adapterCode);
			return;
		}
		String respResult = respInfo.getResult().toString();
		// 取消现有的定时器
		cancel(adapterCode);

		AdapterConfInfo adapterConfInfo = JSON.parseObject(respResult, AdapterConfInfo.class);
		List<PlcPointValueInfo> allPoints = adapterConfInfo.getList();

		AdapterExecutor adapterExecutor = new AdapterExecutor();
		// 点位数据
		Map<String, List<PlcPointValueInfo>> pointMap = adapterExecutor.getPointMap();
		if (MapUtils.isEmpty(pointMap)) {
			pointMap = new HashMap<>();
		}
		for (PlcPointValueInfo e : allPoints) {
			List<PlcPointValueInfo> ipPoints = pointMap.get(e.getIp());
			if (CollectionUtils.isEmpty(ipPoints)) {
				ipPoints = new ArrayList<>();
			}
			ipPoints.add(e);
			pointMap.put(e.getIp(), ipPoints);
		}
		adapterExecutor.setPointMap(pointMap);
		// 时间
		adapterExecutor.setPollingTime(adapterConfInfo.getPollingTime());
		// 放到缓存
		LocalCacheUtil.putAdapterExecutor(adapterCode, adapterExecutor);
		pointTimerComponent.start(adapterCode, isRelay);
	}

	private void cancel(String adapterCode) {
		AdapterExecutor adapterExecutor = LocalCacheUtil.getAdapterExecutor(adapterCode);
		if (adapterExecutor == null) {
			return;
		}
		Integer pollingTime = adapterExecutor.getPollingTime();
		if (pollingTime != null) {
			pollingTime = null;
		}
		Timer timer = adapterExecutor.getTimer();
		if (timer != null) {
			try {
				timer.cancel();
			} catch (Exception e) {
				log.error("取消定时查询异常", e);
			}
			timer = null;
		}
		Map<String, List<PlcPointValueInfo>> pointMap = adapterExecutor.getPointMap();
		if (MapUtils.isNotEmpty(pointMap)) {
			pointMap = null;
		}
		LocalCacheUtil.removeAdapterExecutor(adapterCode);
	}

}
