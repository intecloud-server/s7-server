package com.whl.common.util;

import com.github.xingshuangs.iot.protocol.s7.model.DataItem;
import com.github.xingshuangs.iot.protocol.s7.model.RequestItem;
import com.github.xingshuangs.iot.protocol.s7.utils.AddressUtil;
import com.github.xingshuangs.iot.utils.BooleanUtil;
import com.github.xingshuangs.iot.utils.FloatUtil;
import com.github.xingshuangs.iot.utils.IntegerUtil;
import com.github.xingshuangs.iot.utils.ShortUtil;
import com.whl.common.constants.AppEnumType;
import com.whl.common.constants.AppEnumType.DataType;
import com.whl.common.exception.BusinessException;

public class S7Utils {

	public static RequestItem getRequestItem(String dataTypeStr, String address) {
		DataType dataType = AppEnumType.getByCode(DataType.class, dataTypeStr);
		switch (dataType) {
		case bool:
			return AddressUtil.parseBit(address);
		case double64:// String类型值
			return AddressUtil.parseByte(address, 8);
		case float32:
			return AddressUtil.parseByte(address, 4);
		case int32: // 读到int类型
			return AddressUtil.parseByte(address, 4);
		case short16:
			return AddressUtil.parseByte(address, 2);
		default:
			throw new BusinessException(dataType + "暂不支持的数据类型");
		}
	}

	public static Double getVal(String dataTypeStr, DataItem dataItem) {
		DataType dataType = AppEnumType.getByCode(DataType.class, dataTypeStr);
		switch (dataType) {
		case bool:
			return BooleanUtil.getValue(dataItem.getData()[0], 0) ? 1d : 0d;
		case double64:// String类型值
			return FloatUtil.toFloat64(dataItem.getData());
		case float32:
			return (double) FloatUtil.toFloat32(dataItem.getData());
		case int32: // 读到int类型
			return (double) IntegerUtil.toInt32(dataItem.getData());
		case short16:
			return (double) ShortUtil.toInt16(dataItem.getData());
		default:
			throw new BusinessException(dataType + "暂不支持的数据类型");
		}
	}

}
