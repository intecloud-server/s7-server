package com.whl.project.service.component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.whl.common.util.PageNumUtils;
import com.whl.plc.PlcPointValueInfo;
import com.whl.project.service.component.mqtt.MqttPublishComponent;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PointPublishComponent {
	public static final String TOPIC = "point";
	@Autowired
	MqttPublishComponent mqttPublishComponent;

	public void publish(List<PlcPointValueInfo> points) {
		int totalSize = points.size();
		int pageSize = 200;
		int pageNum = PageNumUtils.getPageNum(totalSize, pageSize);
		for (int i = 0; i < pageNum; i++) {
			Map<String, BigDecimal> map = new HashMap<>();
			int start = i * pageSize;
			int end = (i + 1) * pageSize;
			if (end > totalSize) {
				end = totalSize;
			}
			List<PlcPointValueInfo> subPoints = points.subList(start, end);
			for (PlcPointValueInfo point : subPoints) {
				String key = point.getDeviceCode() + "@" + point.getPointCode();
				map.put(key, point.getPointValue());
			}
			String msg = JSON.toJSONString(map);
			try {
				mqttPublishComponent.execute(0, false, TOPIC, msg);
			} catch (Exception e) {
				log.error("上传数据异常", e);
			}
		}
	}

}
