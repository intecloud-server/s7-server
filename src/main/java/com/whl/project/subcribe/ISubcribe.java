package com.whl.project.subcribe;

import org.eclipse.paho.client.mqttv3.MqttClient;

public interface ISubcribe {
	public abstract String projectPrefix() throws Exception;

	public abstract void subTopic(MqttClient mqttClient) throws Exception;

	public abstract void dealMsg(String topic, String msg) throws Exception;
}
