package com.whl.common.util;

public class PageNumUtils {

	public static int getPageNum(int totalNum, int pageSize) {
		int totalPage = (int) Math.ceil(1.0 * totalNum / pageSize);
		return totalPage;
	}

}
