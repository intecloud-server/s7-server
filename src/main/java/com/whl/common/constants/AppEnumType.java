package com.whl.common.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.whl.common.dto.KeyValueModel;

public class AppEnumType extends EnumType {
	
	public enum PlcType implements IEnumType<String> {
		S200("S200", "S200"),
		S200_SMART("S200_SMART", "S200_SMART"),
		S300("S300", "S300"),
		S400("S400", "S400"),
		S1200("S1200", "S1200"),
		S1500("S1500", "S1500"),
		SINUMERIK_828D("SINUMERIK_828D", "SINUMERIK_828D"),;
		
		private PlcType(String code, String msg) {
			this.code = code;
			this.msg = msg;
		}

		private String code;
		private String msg;

		@Override
		public String code() {
			return code;
		}

		@Override
		public String msg() {
			return msg;
		}
	}
	
	
	public enum DataType implements IEnumType<String> {
		bool("bool", "布尔类型"),
		short16("short", "短整型"),
		int32("int", "整型"),
		float32("float", "浮点类型"),
		double64("double", "双浮点类型"),;
		
		private DataType(String code, String msg) {
			this.code = code;
			this.msg = msg;
		}
		
		private String code;
		private String msg;
		
		@Override
		public String code() {
			return code;
		}
		
		@Override
		public String msg() {
			return msg;
		}
	}
	
	public enum ActionExeType implements IEnumType<String> {
		 fail("fail", "失败"),
		 success("success", "成功"),;

		private ActionExeType(String code, String msg) {
			this.code = code;
			this.msg = msg;
		}

		private String code;
		private String msg;

		@Override
		public String code() {
			return code;
		}

		@Override
		public String msg() {
			return msg;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<KeyValueModel> getByCode(String enumType) {

		String className = AppEnumType.class.getName() + "$" + enumType;
		Class<IEnumType> clazz = null;
		try {
			clazz = (Class<IEnumType>) Class.forName(className);
		} catch (ClassNotFoundException e1) {
			className = EnumType.class.getName() + "$" + enumType;
			try {
				clazz = (Class<IEnumType>) Class.forName(className);
			} catch (ClassNotFoundException e2) {
			}
		}
		if (clazz == null) {
			return new ArrayList<KeyValueModel>();
		}
		IEnumType[] enumConstants = clazz.getEnumConstants();
		List<KeyValueModel> options = new ArrayList<KeyValueModel>(enumConstants.length);
		for (int i = 0; i < enumConstants.length; i++) {
			if (enumConstants[i] != null) {
				KeyValueModel model = new KeyValueModel();
				model.setKey(enumConstants[i].toString());
				model.setValue(enumConstants[i].code());
				model.setLabel(enumConstants[i].msg());
				options.add(model);
			}
		}
		return options;
	}

	@SuppressWarnings({ "rawtypes" })
	public static Map<String, List<KeyValueModel>> getAllAppEnumType() {
		Map<String, List<KeyValueModel>> results = new HashMap<>();
		Class<?>[] innerClazzs = AppEnumType.class.getDeclaredClasses();
		for (Class<?> innerClazz : innerClazzs) {
			IEnumType[] enumConstants = (IEnumType[]) innerClazz.getEnumConstants();
			List<KeyValueModel> options = new ArrayList<>(enumConstants.length);
			for (int i = 0; i < enumConstants.length; i++) {
				if (enumConstants[i] != null) {
					KeyValueModel model = new KeyValueModel();
					model.setKey(enumConstants[i].toString());
					model.setValue(enumConstants[i].code());
					model.setLabel(enumConstants[i].msg());
					options.add(model);
				}
			}
			String innerClazzName = innerClazz.toString();
			String className = innerClazz.toString().substring(innerClazzName.indexOf("$") + 1,
					innerClazzName.length());
			// 首字母转小写
			className = Character.toLowerCase(className.charAt(0)) + (className.substring(1));
			results.put(className, options);
		}
		return results;
	}

}